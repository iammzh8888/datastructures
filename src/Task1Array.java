import java.util.Random;
import java.util.Scanner;

public class Task1Array {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter a number for the array size");

        try{
            int inNum = Integer.parseInt(myObj.nextLine());
            int[] intArray = new int[inNum];
            Random rand = new Random();
            for(int i=0; i<intArray.length;i++){
                intArray[i] = rand.nextInt(100)+1;
            }
            for(int i=0; i<intArray.length;i++){
                if(i!=0){
                    System.out.print(",");
                }
                if(i== intArray.length-1){
                    System.out.println(intArray[i]);
                }
                else {
                    System.out.print(intArray[i]);
                }

            }
            int j = 0;
            for(int i=0; i<intArray.length;i++){
                if (IsPrimeNumber(intArray[i])) {
                    if(j != 0){
                        System.out.print(",");
                    }
                    System.out.print(intArray[i]);
                    j++;
                }
            }

        }catch (Exception exception){
            System.out.println("Enter a positive number");
        }

    }

    private static boolean IsPrimeNumber(int num){
        int i = 2;
        boolean flag = true;
        while (i <= num / 2) {
            // condition for nonprime number
            if (num % i == 0) {
                flag = false;
                break;
            }

            ++i;
        }
        if(num == 1){
            flag = false;
        }
        return flag;
    }
}
