import java.util.Scanner;

public class Task2DArrays {
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] data;

        try{
            System.out.println("Enter the width of the array");
            int width = input.nextInt();
            if (width< 1){
                throw new IllegalArgumentException("Error: the value must be more than 1");
            }
            System.out.println("Enter the height of the array");
            int height = input.nextInt();
            if (height< 1){
                throw new IllegalArgumentException("Error: the value must be more than 1");
            }
            data = new int[width][height];
            for (int i=0; i< data.length; i++){
                for (int j=0; j< data[i].length; j++) {
                    data[i][j] = (int) (100 - Math.random() * 200);
                }
            }

            int sum = 0;
            int[] sumWidth = new int[width];
            int[] sumHeight = new int[height];
            for (int i=0; i< data.length; i++){
                for (int j=0; j< data[i].length; j++) {
                    System.out.printf("%s%3d", j == 0 ? "" : ",", data[i][j]);
                    sum += data[i][j];
                    sumWidth[i] += data[i][j];
                    sumHeight[j] += data[i][j];
                }
                System.out.println();
            }

            System.out.println("Sum of all numbers: " + sum);
            System.out.println("Sum of each of the row of the array: ");
            for(int i=0; i< sumWidth.length; i++){
                System.out.printf("%s%d", i == 0 ? "" : ",", sumWidth[i]);
            }
            System.out.println();
            System.out.println("Sum of each of the column of the array: ");
            for(int j=0; j< sumHeight.length; j++){
                System.out.printf("%s%d", j == 0 ? "" : ",", sumHeight[j]);
            }
            System.out.println();
            //deviation
            double sumDeviation = 0;
            double average = (double)sum/width/height;
            for (int i=0; i< data.length; i++){
                for (int j=0; j< data[i].length; j++) {
                    sumDeviation += Math.pow((data[i][j] - average),2);
                }
            }
            double deviation = Math.sqrt(sumDeviation/width/height);
            System.out.println("Standard deviation of all numbers in the array: " + deviation);

            System.out.println("Pairs of numbers in the array whose sum is a prime number: ");
            System.out.println("rows: ");
            for (int i=0; i< sumWidth.length; i++){
                //I will create a method to check if the number is prime
                if (isPrime(sumWidth[i])){
                    for(int j=0; j< sumHeight.length; j++){
                        System.out.printf("%s%d", j == 0 ? "Row " + (i+1) + ": " : ",", data[i][j]);
                    }
                    System.out.print(":" + sumWidth[i]);
                    System.out.println();
                }
            }
            System.out.println("columns: ");
            for (int j=0; j< sumHeight.length; j++){
                //I will create a method to check if the number is prime
                if (isPrime(sumHeight[j])){
                    for(int i=0; i< sumWidth.length; i++){
                        System.out.printf("%s%d", i == 0 ? "Column " + (j+1) + ": " : ",", data[i][j]);
                    }
                    System.out.print(":" + sumHeight[j]);
                    System.out.println();
                }
            }
        }
        catch(IllegalArgumentException exc){
            System.out.println(exc.getMessage());
        }
    }

    private static boolean isPrime(int number){

        for (int i=2;i<= Math.sqrt(number); i++){
            if (number % i == 0){
                return false;
            }
        }
        if (number < 2){
            return false;
        }
        return true;
    }
}